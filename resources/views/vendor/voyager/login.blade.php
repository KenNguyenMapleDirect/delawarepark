@extends('layouts.app')

@section('content')
    <div class="container" style="text-align: center; background: #fff; margin: 0 auto;">
        <div class="row justify-content-center">
            <div class="col-md-12">
                <div class="card-body">
                    <!-- MOS Start -->
                    <div id="mosContainer">

                        <div id="mosHeader">
                        </div>

                        <div id="mosBody">
                            <div id="mosCenter">

                                <div id="mosContentWrapper" class="mosCenterText">
                                    <hr class="mosTemplateHR-top">
                                    <div id="mosContent" class="mosCenterText mosTopLineSpacer">&nbsp;</div>

                                    <div id="mosContentTitle">Welcome to your Online Portal Review site</div>

                                    <div id="mosContent" class="mosCenterText">&nbsp;</div>
                                    @if(session()->has('message'))
                                        <div class="alert alert-success">
                                            {{ session()->get('message') }}
                                        </div>
                                    @endif
                                    <div id="mosContent">
                                        <p>
                                            After logging in with your User ID and Password
                                            you can enter an Account ID to review and approve
                                            Player offers.
                                        </p>
                                    </div>
                                    <div id="mosLoginForm">
                                        @if($errors->has('email') || $errors->has('user_name'))
                                            <span
                                                style="color: red">{{$errors->first('email') }} {{ $errors->first('user_name')}}</span>
                                        @endif
                                        <form name="loginForm" action="{{ route('voyager.login') }}" method="POST">
                                            {{ csrf_field() }}
                                            <div id="mosLoginFormLine0">
                                                <div id="mosLoginFormLine">
                                                    <label for="uid">User Name or Email:</label>
                                                    <input name="user_name" id="user_name" type="text"
                                                           value="{{ old('user_name') }}"
                                                           class="form-control @if($errors->has('email') || $errors->has('user_name')) has-error @endif"
                                                           required>
                                                </div>
                                                <div id="mosLoginFormLine">
                                                    <label for="upwd">Password:</label>
                                                    <input id="upwd" value="" name="password" type="password"
                                                           class="form-control"
                                                           required>
                                                </div>
                                                <div class="form-group" id="rememberMeGroup">
                                                    <div class="controls">
                                                        <a href="/forget-password">Forget password!</a>
                                                    </div>
                                                </div>


                                                <div class="form-group" id="rememberMeGroup">
                                                    <div class="controls">
                                                        <input type="checkbox" name="remember" id="remember"
                                                               value="1"><label for="remember"
                                                                                class="remember-me-text">{{ __('voyager::generic.remember_me') }}</label>
                                                    </div>
                                                </div>
                                                <div id="mosLoginButton">
                                                    <input value="Login" type="submit">
                                                </div>
                                                <div style="clear:both;"></div>
                                            </div>
                                        </form>

                                    </div>
                                    <div style="clear:both;"></div>

                                </div>
                            </div>

                            <div id="mosFooter" style="display: none;">
                                <div id="MOS-footer">
                                    Powered By Maple Online Services
                                </div>
                            </div>
                        </div>

                    </div>
                    <!-- MOS End   -->
                </div>

            </div>
            <div class="row" style="padding-bottom: 30px">
                <div class="col-sm-6"><a href="register">Do not have an account? sign up now!</a></div>
                <div class="col-sm-6"> Powered By Maple Web Services
                </div>
            </div>
        </div>
@endsection
