@extends('layouts.app')

@section('content')
        <div class="row justify-content-center" style="padding-top: 20px;background: white">
            <div class="col-md-9">
                    </br>
            </div>
            <div class="col-md-3">
                <a href="/" rel="nofollow"> <!-- Bally&#039;s Atlantic City Hotel and Casino</a> -->
                    @switch($data->BAC_Tier)
                        @case('Red')
                        <img src="{{ asset('assets/images/cards/red.png') }}" alt="bally's card"></a>
                <h4 style="font-weight: 600;color: #232325;">
                    Welcome, {{ $data->first_name }} {{  $data->last_name}}</h4>
                <h5 style="color:red;text-align: center;"
                    onclick="event.preventDefault(); document.getElementById('logout-form').submit();"><a
                        href="https://ballysac.maplewebservices.com/admin/logout" style="color:red">Logout</a></h5>
                <div id="TierLevel" class="textlineRed">Tier Level Red</div>
                {{--                <hr style="height:20px;border:none;color:red;background-color:red;"/>--}}
                <div id="yourMembershipLevel">
                    <div id="pointsBalanceNextLevel">Tier Score <span
                            style="color:red;">{{  number_format($data->BAC_Tier_Points ?? 0) }}</span>
                    </div>
                    <div id="pointsBalanceNextLevel"> Reward Points <span
                            style="color:red;">${{  $data->BAC_Reward_Points?substr_replace($data->BAC_Reward_Points,'.',strlen($data->BAC_Reward_Points)-2,0):0 }}</span>
                        <br><span style="font-size: 12px;font-style: italic;color: darkgrey;"><i
                                class="fas fa-info-circle"></i> Data updated at {{$data->updated_at}}</span>
                    </div>
                </div>
                {{--            <div id="pointsBalance">--}}
                {{--                <div id="pointsBalanceNextLevelAmt">--}}
                {{--                   <span style="color:red;"> {{  $data->BAC_Points_Next_Tier ?? 0 }}  </span>Tier Credits to advance to--}}
                {{--</div>--}}
                {{--                <div id="cardNextLevel">--}}
                {{--                </div>--}}
                {{--            </div>--}}
                <br>
                <hr style="height:1px;border:none;color:red;background-color:red;"/>
                @break
                @case('Black')
                <img src="{{ asset('assets/images/cards/black.png') }}" alt="bally's card"></a>
                <h4 style="font-weight: 600;color: #232325;">
                    Welcome, {{ $data->first_name }} {{  $data->last_name}}</h4>
                <h5 style="color:red;text-align: center;"
                    onclick="event.preventDefault(); document.getElementById('logout-form').submit();"><a
                        href="https://ballysac.maplewebservices.com/admin/logout" style="color:red">Logout</a></h5>
                <div id="TierLevel" class="textlineBlack">Tier Level Black</div>
                {{--                <hr style="height:20px;border:none;color:red;background-color:black;"/>--}}
                <div id="yourMembershipLevel">
                    <div id="pointsBalanceNextLevel">Tier Score <span
                            style="color:red;">{{  number_format($data->BAC_Tier_Points ?? 0) }}</span>
                    </div>
                    <div id="pointsBalanceNextLevel"> Reward Points <span
                            style="color:red;">${{  $data->BAC_Reward_Points?substr_replace($data->BAC_Reward_Points,'.',strlen($data->BAC_Reward_Points)-2,0):0 }}</span>
                        <br><span style="font-size: 12px;font-style: italic;color: darkgrey;"><i
                                class="fas fa-info-circle"></i> Data updated at {{$data->updated_at}}</span>
                    </div>
                </div>
                {{--            <div id="pointsBalance">--}}
                {{--                <div id="pointsBalanceNextLevelAmt">--}}
                {{--                    <span style="color:red;"> {{  $data->BAC_Points_Next_Tier ?? 0 }}  </span>Tier Credits to advance to--}}
                {{--                    Red--}}
                {{--                </div>--}}
                {{--                <div id="cardNextLevel">--}}
                {{--                </div>--}}
                {{--            </div>--}}
                <br>
                <hr style="height:1px;border:none;color:red;background-color:black;"/>
                @break
                @case('Platinum')
                <img src="{{ asset('assets/images/cards/platinum.png') }}" alt="bally's card"></a>
                <h4 style="font-weight: 600;color: #232325;">
                    Welcome, {{ $data->first_name }} {{  $data->last_name}}</h4>
                <h5 style="color:red;text-align: center;"
                    onclick="event.preventDefault(); document.getElementById('logout-form').submit();"><a
                        href="https://ballysac.maplewebservices.com/admin/logout" style="color:red">Logout</a></h5>
                <div id="TierLevel" class="textlinePlatinum">Tier Level Platinum</div>
                {{--                <hr style="height:20px;border:none;color:red;background-color:#e5e4e2;"/>--}}
                <div id="yourMembershipLevel">
                    <div id="pointsBalanceNextLevel">Tier Score <span
                            style="color:red;">{{  number_format($data->BAC_Tier_Points ?? 0) }}</span>
                    </div>
                    <div id="pointsBalanceNextLevel"> Reward Points <span
                            style="color:red;">${{  $data->BAC_Reward_Points?substr_replace($data->BAC_Reward_Points,'.',strlen($data->BAC_Reward_Points)-2,0):0 }}</span>
                        <br><span style="font-size: 12px;font-style: italic;color: darkgrey;"><i
                                class="fas fa-info-circle"></i> Data updated at {{$data->updated_at}}</span>
                    </div>
                </div>
                {{--            <div id="pointsBalance">--}}
                {{--                <div id="pointsBalanceNextLevelAmt">--}}
                {{--                    <span style="color:red;"> {{  $data->BAC_Points_Next_Tier ?? 0 }}  </span>Tier Credits to advance to--}}
                {{--                    Black--}}
                {{--                </div>--}}
                {{--                <div id="cardNextLevel">--}}
                {{--                </div>--}}
                {{--            </div>--}}
                <br>
                <hr style="height:1px;border:none;color:red;background-color:#e5e4e2;"/>
                @break
                @default
                <img src="{{ asset('assets/images/cards/gold.png') }}" alt="bally's card"></a>
                <h4 style="font-weight: 600;color: #232325;">
                    Welcome, {{ $data->first_name }} {{  $data->last_name}}</h4>
                <h5 style="color:red;text-align: center;"
                    onclick="event.preventDefault(); document.getElementById('logout-form').submit();"><a
                        href="https://ballysac.maplewebservices.com/admin/logout" style="color:red">Logout</a></h5>
                <div id="TierLevel" class="textlineGold">Tier Level Gold</div>
                {{--                <hr style="height:20px;border:none;color:red;background-color:#FFD700;"/>--}}
                <div id="yourMembershipLevel">
                    <div id="pointsBalanceNextLevel">Tier Score <span
                            style="color:red;">{{  number_format($data->BAC_Tier_Points ?? 0) }}</span>
                    </div>
                    <div id="pointsBalanceNextLevel"> Reward Points <span
                            style="color:red;">${{  $data->BAC_Reward_Points?substr_replace($data->BAC_Reward_Points,'.',strlen($data->BAC_Reward_Points)-2,0):0 }}</span>
                        <br><span style="font-size: 12px;font-style: italic;color: darkgrey;"><i
                                class="fas fa-info-circle"></i> Data updated at {{$data->updated_at}}</span>
                    </div>
                </div>
                {{--            <div id="pointsBalance">--}}
                {{--                <div id="pointsBalanceNextLevelAmt">--}}
                {{--                    <span style="color:red;"> {{  $data->BAC_Points_Next_Tier ?? 0 }}  </span>Tier Credits to advance to--}}
                {{--                    Platinum--}}
                {{--                </div>--}}
                {{--                <div id="cardNextLevel">--}}
                {{--                </div>--}}
                {{--            </div>--}}
                <br>
                <hr style="height:1px;border:none;color:red;background-color:#FFD700;"/>
                @endswitch
            </div>
        </div>
@endsection


