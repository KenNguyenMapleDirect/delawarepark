<?php

namespace App\Http\Controllers\Voyager;

use App\Data;
use App\Flipbook;
use App\User;
use App\WeekenderFlipbook;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\File;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Str;
use Intervention\Image\Constraint;
use Intervention\Image\Facades\Image;
use League\Flysystem\Util;
use TCG\Voyager\Facades\Voyager;
use TCG\Voyager\Http\Controllers\Controller;

class VoyagerController extends \TCG\Voyager\Http\Controllers\VoyagerController
{
    public function index()
    {
        if (Auth::user()->role_id === 1 || Auth::user()->role_id === 4)
            return Voyager::view('voyager::index');
        else {
            //Get User from user Id
            $user = User::where('id', Auth::user()->id)->first();
            //define data
            $data = new \stdClass();
            $data->first_name = $user->first_name;
            $data->last_name = $user->last_name;
            $data->BAC_Tier = '';
            $data->BAC_Reward_Points = 0;
            $data->BAC_Tier_Points = 0;
            $data->BAC_Account_Number = $user->BAC_Account_Number;
            $data->Flipbook_Account = '';
            $data->Weekender_Flipbook_Account = '';
            $data->updated_at = now();
            //Update user data and flipbook data
            $datas = Data::where('BAC_Account_Combined', Auth::user()->BAC_Account_Combined)->first();
            if ($datas) {
                $data->first_name = $datas->first_name;
                $data->last_name = $datas->last_name;
                $data->BAC_Tier = $datas->BAC_Tier;
                $data->BAC_Reward_Points = $datas->BAC_Reward_Points;
                $data->BAC_Tier_Points = $datas->BAC_Tier_Points;
                $data->BAC_Points_Next_Tier = $datas->BAC_Points_Next_Tier;
                $data->BAC_Account_Number = $datas->BAC_Account_Number;
                $data->BAC_Temp_Account_Number = $datas->BAC_Temp_Account_Number;
                $data->BAC_Account_Combined = $datas->BAC_Account_Combined;
                $data->BAC_Player_ID = $datas->BAC_Player_ID;
                $data->BAC_MI = $datas->BAC_MI;
                $data->BAC_Host = $datas->BAC_Host;
                $data->BAC_Host_ID = $datas->BAC_Host_ID;
            }
            //Get data from May_Test table
            //Define data type flag
            $data->coreSM = 0;
            $data->corePC = 0;
            $data->cruisePC = 0;
            $data->weekenderPC = 0;
            $dataFromMay2021 = Flipbook::where('BAC_Account',$data->BAC_Account_Number)->get();

            if($dataFromMay2021)
            {
                foreach($dataFromMay2021 as $singleDataFromMay2021)
                {
                    if($singleDataFromMay2021->BAC_Mailer_Type === "Core SM"){
                        $data->coreSM = 1;
                        $data->result1 = $singleDataFromMay2021->BAC_Img_Page01.".jpg";
                        $data->result2 = $singleDataFromMay2021->BAC_Img_Page02.".jpg";
                        $data->result3 = $singleDataFromMay2021->BAC_Img_Page03.".jpg";
                        $data->result4 = $singleDataFromMay2021->BAC_Img_Page04.".jpg";
                        $data->result5 = $singleDataFromMay2021->BAC_Img_Page05.".jpg";
                        $data->result6 = $singleDataFromMay2021->BAC_Img_Page06.".jpg";
                    }

                    if($singleDataFromMay2021->BAC_Mailer_Type === "Core PC"){
                        $data->corePC = 1;
                        $data->result1 = $singleDataFromMay2021->BAC_Img_Page01.".jpg";
                        $data->result2 = $singleDataFromMay2021->BAC_Img_Page02.".jpg";

                    }

                    if($singleDataFromMay2021->BAC_Mailer_Type === "CRUISE PC"){
                        $data->cruisePC = 1;
                        $data->cruisePCResult1 = $singleDataFromMay2021->BAC_Img_Page01.".jpg";
                        $data->cruisePCResult2 = $singleDataFromMay2021->BAC_Img_Page02.".jpg";

                    }

                    if($singleDataFromMay2021->BAC_Mailer_Type === "WEEKENDER PC"){
                        $data->weekenderPC = 1;
                        $data->weekenderPCResult1 = $singleDataFromMay2021->BAC_Img_Page01.".jpg";
                        $data->weekenderPCResult2 = $singleDataFromMay2021->BAC_Img_Page02.".jpg";

                    }
                }


            }

            return view('player-dashboard')->with('data', $data);
        }
    }

    //code for superUser here
    public function getViewPlayerDashBoardByAccountId($accountId)
    {
        //Get User from user Id
        $user = Data::where('BAC_Account_Combined', $accountId)->first();
        //get user data and flipbook data
        $data = new \stdClass();
        $data->Flipbook_Account = '';
        $data->Weekender_Flipbook_Account = '';
        $data->first_name = $user->BAC_FName;
        $data->last_name = $user->BAC_Lname;
        $data->BAC_Tier = $user->BAC_Tier;
        $data->BAC_Reward_Points = $user->BAC_Reward_Points;
        $data->BAC_Tier_Points = $user->BAC_Tier_Points;
        $data->BAC_Reward_Points = $user->BAC_Reward_Points;
        $data->BAC_Tier_Points = $user->BAC_Tier_Points;
        $data->BAC_Points_Next_Tier = $user->BAC_Points_Next_Tier;
        $data->BAC_Account_Number = $user->BAC_Account_Number;
        $data->BAC_Temp_Account_Number = $user->BAC_Temp_Account_Number;
        $data->BAC_Account_Combined = $user->BAC_Account_Combined;
        $data->BAC_Player_ID = $user->BAC_Player_ID;
        $data->BAC_MI = $user->BAC_MI;
        $data->BAC_Host = $user->BAC_Host;
        $data->BAC_Host_ID = $user->BAC_Host_ID;
        $data->updated_at = now();

        //Get data from May_Test table
            //Define data type flag
            $data->coreSM = 0;
            $data->corePC = 0;
            $data->cruisePC = 0;
            $data->weekenderPC = 0;
            $dataFromMay2021 = Flipbook::where('BAC_Account',$data->BAC_Account_Number)->get();

            if($dataFromMay2021)
            {
                foreach($dataFromMay2021 as $singleDataFromMay2021)
                {
                    if($singleDataFromMay2021->BAC_Mailer_Type === "Core SM"){
                        $data->coreSM = 1;
                        $data->result1 = $singleDataFromMay2021->BAC_Img_Page01.".jpg";
                        $data->result2 = $singleDataFromMay2021->BAC_Img_Page02.".jpg";
                        $data->result3 = $singleDataFromMay2021->BAC_Img_Page03.".jpg";
                        $data->result4 = $singleDataFromMay2021->BAC_Img_Page04.".jpg";
                        $data->result5 = $singleDataFromMay2021->BAC_Img_Page05.".jpg";
                        $data->result6 = $singleDataFromMay2021->BAC_Img_Page06.".jpg";
                    }

                    if($singleDataFromMay2021->BAC_Mailer_Type === "Core PC"){
                        $data->corePC = 1;
                        $data->result1 = $singleDataFromMay2021->BAC_Img_Page01.".jpg";
                        $data->result2 = $singleDataFromMay2021->BAC_Img_Page02.".jpg";

                    }

                    if($singleDataFromMay2021->BAC_Mailer_Type === "CRUISE PC"){
                        $data->cruisePC = 1;
                        $data->cruisePCResult1 = $singleDataFromMay2021->BAC_Img_Page01.".jpg";
                        $data->cruisePCResult2 = $singleDataFromMay2021->BAC_Img_Page02.".jpg";

                    }

                    if($singleDataFromMay2021->BAC_Mailer_Type === "WEEKENDER PC"){
                        $data->weekenderPC = 1;
                        $data->weekenderPCResult1 = $singleDataFromMay2021->BAC_Img_Page01.".jpg";
                        $data->weekenderPCResult2 = $singleDataFromMay2021->BAC_Img_Page02.".jpg";

                    }
                }


            }

        return view('player-dashboard')->with('data', $data);
    }

    public function logout()
    {
        Auth::logout();

        return redirect()->route('voyager.login');
    }

    public function upload(Request $request)
    {
        $fullFilename = null;
        $resizeWidth = 1800;
        $resizeHeight = null;
        $slug = $request->input('type_slug');
        $file = $request->file('image');

        $path = $slug . '/' . date('F') . date('Y') . '/';

        $filename = basename($file->getClientOriginalName(), '.' . $file->getClientOriginalExtension());
        $filename_counter = 1;

        // Make sure the filename does not exist, if it does make sure to add a number to the end 1, 2, 3, etc...
        while (Storage::disk(config('voyager.storage.disk'))->exists($path . $filename . '.' . $file->getClientOriginalExtension())) {
            $filename = basename($file->getClientOriginalName(), '.' . $file->getClientOriginalExtension()) . (string)($filename_counter++);
        }

        $fullPath = $path . $filename . '.' . $file->getClientOriginalExtension();

        $ext = $file->guessClientExtension();

        if (in_array($ext, ['jpeg', 'jpg', 'png', 'gif'])) {
            $image = Image::make($file)
                ->resize($resizeWidth, $resizeHeight, function (Constraint $constraint) {
                    $constraint->aspectRatio();
                    $constraint->upsize();
                });
            if ($ext !== 'gif') {
                $image->orientate();
            }
            $image->encode($file->getClientOriginalExtension(), 75);

            // move uploaded file from temp to uploads directory
            if (Storage::disk(config('voyager.storage.disk'))->put($fullPath, (string)$image, 'public')) {
                $status = __('voyager::media.success_uploading');
                $fullFilename = $fullPath;
            } else {
                $status = __('voyager::media.error_uploading');
            }
        } else {
            $status = __('voyager::media.uploading_wrong_type');
        }

        // echo out script that TinyMCE can handle and update the image in the editor
        return "<script> parent.helpers.setImageValue('" . Voyager::image($fullFilename) . "'); </script>";
    }
}
