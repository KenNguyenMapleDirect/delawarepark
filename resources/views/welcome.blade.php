
<nav class="fusion-mobile-nav-holder fusion-mobile-menu-text-align-center">
    <ul id="mobile-menu-main-menu-2" class="fusion-mobile-menu">
        <li role="menuitem" class="fusion-mobile-nav-item btn-main" data-classes="btn-main"
            style=""><a href="http://www.delawarepark.com/"
                        class="fusion-top-level-link fusion-bar-highlight"><span
                    class="menu-text"><span class="fusion-button-text-left">Home</span></span></a>
        </li>
        <li role="menuitem" class="fusion-mobile-nav-item menu-item-has-children" style=""><span
                href="#" aria-haspopup="true" class="fusion-open-submenu"></span><a
                href="http://www.delawarepark.com/racing/"
                class="fusion-top-level-link fusion-bar-highlight"><span
                    class="menu-text">Racing</span> </a>
            <ul role="menu" class="sub-menu" style="display: none;">
                <li role="menuitem" class="fusion-mobile-nav-item" data-width="319.99872" style="">
                    <a href="http://www.delawarepark.com/racing/race-info/">Live Racing</a></li>
                <li role="menuitem" class="fusion-mobile-nav-item" data-width="319.99872" style="">
                    <a href="http://www.equibase.com/static/entry/DEL-calendar.html">Entries</a>
                </li>
            </ul>
            <ul role="menu" class="sub-menu" style="display: none;">
                <li role="menuitem" class="fusion-mobile-nav-item" data-width="319.99872" style="">
                    <a href="http://www.equibase.com/static/chart/summary/DEL-calendar.html">Results</a>
                </li>
                <li role="menuitem" class="fusion-mobile-nav-item" data-width="319.99872" style="">
                    <a href="http://www.delawarepark.com/racing/horsemen/">Horsemen</a></li>
            </ul>
            <ul role="menu" class="sub-menu" style="display: none;">
                <li role="menuitem" class="fusion-mobile-nav-item" data-width="319.99872" style="">
                    <a href="http://www.delawarepark.com/racing/simulcast/">Simulcast</a></li>
                <li role="menuitem" class="fusion-mobile-nav-item" data-width="319.99872" style="">
                    <a href="http://www.delawarepark.com/racing/track-records/">Track Records</a>
                </li>
            </ul>
            <ul role="menu" class="sub-menu" style="display: none;">
                <li role="menuitem" class="fusion-mobile-nav-item" data-width="319.99872" style="">
                    <a href="http://www.delawarepark.com/racing/statistics/">Statistics</a></li>
                <li role="menuitem" class="fusion-mobile-nav-item" data-width="319.99872" style="">
                    <a href="http://www.delawarepark.com/racing/audio-video/">Live A/V</a></li>
            </ul>
            <ul role="menu" class="sub-menu" style="display: none;">
                <li role="menuitem" class="fusion-mobile-nav-item" data-width="319.99872" style="">
                    <a href="http://www.delawarepark.com/racing/wagering/">Wagering</a></li>
                <li role="menuitem" class="fusion-mobile-nav-item" data-width="319.99872" style="">
                    <a href="http://www.delawarepark.com/racing/faq/">Racing FAQ</a></li>
            </ul>
        </li>
        <li role="menuitem" class="fusion-mobile-nav-item menu-item-has-children" style=""><span
                href="#" aria-haspopup="true" class="fusion-open-submenu"></span><a
                href="http://www.delawarepark.com/casino/"
                class="fusion-top-level-link fusion-bar-highlight"><span
                    class="menu-text">Casino</span> </a>
            <ul role="menu" class="sub-menu" style="display: none;">
                <li role="menuitem" class="fusion-mobile-nav-item" data-width="319.99872" style="">
                    <a href="http://www.delawarepark.com/casino/table-games/">Table Games</a></li>
                <li role="menuitem" class="fusion-mobile-nav-item" data-width="319.99872" style="">
                    <a href="http://www.delawarepark.com/casino/keno/">Keno</a></li>
            </ul>
            <ul role="menu" class="sub-menu" style="display: none;">
                <li role="menuitem" class="fusion-mobile-nav-item fusion-mobile-current-nav-item"
                    data-width="319.99872" style=""><a
                        href="http://www.delawarepark.com/casino/slots/">Slots</a></li>
                <li role="menuitem" class="fusion-mobile-nav-item" data-width="319.99872" style="">
                    <a href="http://www.delawarepark.com/casino/casino-tournaments/">Tournaments</a>
                </li>
            </ul>
            <ul role="menu" class="sub-menu" style="display: none;">
                <li role="menuitem" class="fusion-mobile-nav-item" data-width="319.99872" style="">
                    <a href="http://www.delawarepark.com/casino/casino-promotions/">Promotions</a>
                </li>
                <li role="menuitem" class="fusion-mobile-nav-item" data-width="319.99872" style="">
                    <a href="http://www.delawarepark.com/casino/free-entertainment/">Free
                        Entertainment</a></li>
            </ul>
            <ul role="menu" class="sub-menu" style="display: none;">
                <li role="menuitem" class="fusion-mobile-nav-item" data-width="319.99872" style="">
                    <a href="http://www.delawarepark.com/casino/casino-age-restrictions/">Age
                        Restrictions</a></li>
            </ul>
        </li>
        <li role="menuitem" class="fusion-mobile-nav-item menu-item-has-children" style=""><span
                href="#" aria-haspopup="true" class="fusion-open-submenu"></span><a
                href="http://www.delawarepark.com/sports-betting/"
                class="fusion-top-level-link fusion-bar-highlight"><span
                    class="menu-text">Sports</span> </a>
            <ul role="menu" class="sub-menu" style="display: none;">
                <li role="menuitem" class="fusion-mobile-nav-item" style=""><a
                        href="http://www.delawarepark.com/sports-betting/guide/"
                        class="fusion-bar-highlight"><span>Guide</span></a></li>
            </ul>
        </li>
        <li role="menuitem" class="fusion-mobile-nav-item" style=""><a
                href="http://www.delawarepark.com/poker/"
                class="fusion-top-level-link fusion-bar-highlight"><span
                    class="menu-text">Poker</span></a></li>
        <li role="menuitem" class="fusion-mobile-nav-item" style=""><a target="_blank"
                                                                       rel="noopener noreferrer"
                                                                       href="http://www.whiteclaycreek.com/"
                                                                       class="fusion-top-level-link fusion-bar-highlight"><span
                    class="menu-text">Golf</span></a></li>
        <li role="menuitem" class="fusion-mobile-nav-item" style=""><a
                href="http://www.delawarepark.com/player-rewards/"
                class="fusion-top-level-link fusion-bar-highlight"><span
                    class="menu-text">Rewards</span></a></li>
        <li role="menuitem" class="fusion-mobile-nav-item" style=""><a target="_blank"
                                                                       rel="noopener noreferrer"
                                                                       href="http://www.delawarepark.com/igaming"
                                                                       class="fusion-top-level-link fusion-bar-highlight"><span
                    class="menu-text">Online Gaming</span></a></li>
        <li role="menuitem" class="fusion-mobile-nav-item menu-item-has-children" style=""><span
                href="#" aria-haspopup="true" class="fusion-open-submenu"></span><a
                href="http://www.delawarepark.com/dining/"
                class="fusion-top-level-link fusion-bar-highlight"><span
                    class="menu-text">Dining</span> </a>
            <ul role="menu" class="sub-menu" style="display: none;">
                <li role="menuitem" class="fusion-mobile-nav-item" data-width="319.99872" style="">
                    <a href="http://www.delawarepark.com/dining/legends/">Legends</a></li>
                <li role="menuitem" class="fusion-mobile-nav-item" data-width="319.99872" style="">
                    <a href="http://www.delawarepark.com/dining/new-castle/">New Castle</a></li>
            </ul>
            <ul role="menu" class="sub-menu" style="display: none;">
                <li role="menuitem" class="fusion-mobile-nav-item" data-width="319.99872" style="">
                    <a href="http://www.delawarepark.com/dining/picciottis-pizza/">Picciotti’s
                        Pizza</a></li>
                <li role="menuitem" class="fusion-mobile-nav-item" data-width="319.99872" style="">
                    <a href="http://www.delawarepark.com/dining/delcap/">Del’Cap</a></li>
            </ul>
            <ul role="menu" class="sub-menu" style="display: none;">
                <li role="menuitem" class="fusion-mobile-nav-item" data-width="319.99872" style="">
                    <a href="http://www.delawarepark.com/dining/at-the-rail/">At the Rail</a></li>
                <li role="menuitem" class="fusion-mobile-nav-item" data-width="319.99872" style="">
                    <a href="http://www.delawarepark.com/dining/on-a-roll/">On A Roll Deli</a></li>
            </ul>
            <ul role="menu" class="sub-menu" style="display: none;">
                <li role="menuitem" class="fusion-mobile-nav-item" data-width="319.99872" style="">
                    <a href="http://www.delawarepark.com/dining/rooneys/">Rooney’s</a></li>
                <li role="menuitem" class="fusion-mobile-nav-item" data-width="319.99872" style="">
                    <a href="http://www.delawarepark.com/dining/kelsos/">Kelso’s</a></li>
            </ul>
            <ul role="menu" class="sub-menu" style="display: none;">
                <li role="menuitem" class="fusion-mobile-nav-item" data-width="319.99872" style="">
                    <a href="http://www.delawarepark.com/dining/sports-book/">Sports Book</a></li>
                <li role="menuitem" class="fusion-mobile-nav-item" data-width="319.99872" style="">
                    <a href="http://www.delawarepark.com/dining/the-grove/">The Grove</a></li>
            </ul>
            <ul role="menu" class="sub-menu" style="display: none;">
                <li role="menuitem" class="fusion-mobile-nav-item" data-width="319.99872" style="">
                    <a href="http://www.delawarepark.com/dining/terrace/">Terrace</a></li>
                <li role="menuitem" class="fusion-mobile-nav-item" data-width="319.99872" style="">
                    <a href="http://www.delawarepark.com/dining/banquet/">Banquets</a></li>
            </ul>
        </li>
        <li role="menuitem" class="fusion-mobile-nav-item btn-main" data-classes="btn-main"
            style=""><a href="http://www.delawarepark.com/directions/"
                        class="fusion-top-level-link fusion-bar-highlight"><span
                    class="menu-text"><span class="fusion-button-text-left">Directions</span></span></a>
        </li>
        <li role="menuitem" class="fusion-mobile-nav-item btn-main" data-classes="btn-main"
            style=""><a href="http://www.delawarepark.com/about-us/"
                        class="fusion-top-level-link fusion-bar-highlight"><span
                    class="menu-text"><span
                        class="fusion-button-text-left">About Us</span></span></a></li>
        <li role="menuitem" class="fusion-mobile-nav-item btn-main" data-classes="btn-main"
            style=""><a href="http://www.delawarepark.com/contact-us/"
                        class="fusion-top-level-link fusion-bar-highlight"><span
                    class="menu-text"><span class="fusion-button-text-left">Contact Us</span></span></a>
        </li>
    </ul>
</nav>
