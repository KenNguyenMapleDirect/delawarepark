<?php

namespace App\Http\Controllers\Auth;

use App\Data;
use App\Flipbook;
use App\Http\Controllers\Controller;
use App\Mail\WelcomeMail;
use Illuminate\Http\Request;
use App\User;
use Hash;
use Illuminate\Validation\ValidationException;
use Mail;

class RegisterController extends Controller
{
    public function register()
    {

        return view('auth.register');
    }

    public function storeUser(Request $request)
    {
        $request->validate([
            'first_name' => 'required|string|max:255',
            'last_name' => 'required|string|max:255',
            'user_name' => 'required|string|max:255|unique:users',
            'birthday' => 'required',
            'email' => 'required|string|email|max:255|unique:users',
            'BAC_Account_Combined' => 'required|string|max:255|unique:users',
            'password' => 'required|string|min:8|confirmed',
            'password_confirmation' => 'required',
        ]);

        //check if user register right birthday and BAC_Account_Combined
        $checkAccount = Data::where('BAC_Account_Combined', $request->BAC_Account_Combined)
            ->where('BAC_DOB', $request->birthday)->first();
        if(!$checkAccount)
        {
            throw ValidationException::withMessages(['No_Match_Up' => 'Your Account Combined ID or Birthday does not match our records
. Please contact guest services.']);
        }
//        //get flipbook data of user
        if($checkAccount->BAC_Account_Number)
        $flipbookData = Flipbook::where('Flipbook_Account',$checkAccount->BAC_Account_Number)->first();

        if($flipbookData)
        {
            User::create([
                'first_name' => $request->first_name,
                'last_name' => $request->last_name,
                'user_name' => $request->user_name,
                'email' => $request->email,
                'role_id' => 2,
                'password' => Hash::make($request->password),
                'BAC_Account_Combined' => $request->BAC_Account_Combined,
                'BAC_Player_ID' => $checkAccount->BAC_Player_ID,
                'BAC_Lname' => $checkAccount->BAC_Lname,
                'BAC_FName' => $checkAccount->BAC_FName,
                'BAC_MI' => $checkAccount->BAC_MI,
                'BAC_Tier' => $checkAccount->BAC_Tier,
                'BAC_Reward_Points' => $checkAccount->BAC_Reward_Points,
                'BAC_Tier_Points' => $checkAccount->BAC_Tier_Points,
                'BAC_Points_Next_Tier' => $checkAccount->BAC_Points_Next_Tier,
                'BAC_Host' => $checkAccount->BAC_Host,
                'BAC_Account_Number' => $checkAccount->BAC_Account_Number,
                'BAC_Temp_Account_Number' => $checkAccount->BAC_Temp_Account_Number,
                'BAC_Host_ID' => $checkAccount->BAC_Host_ID,
                'BAC_DOB' => $request->birthday,
                'Flipbook_Account' => $flipbookData->Flipbook_Account,
                'Flipbook_FName' => $flipbookData->Flipbook_FName,
                'Flipbook_LName' => $flipbookData->Flipbook_LName,
                'Flipbook_Tier' => $flipbookData->Flipbook_Tier,
                'Flipbook_Version' => $flipbookData->Flipbook_Version,
                'Flipbook_FP' => $flipbookData->Flipbook_FP,
                'Flipbook_Total_FP' => $flipbookData->Flipbook_Total_FP,
                'Flipbook_Food' => $flipbookData->Flipbook_Food,
                'Flipbook_SBFP' => $flipbookData->Flipbook_SBFP,
                'Flipbook_GC' => $flipbookData->Flipbook_GC,
                'Flipbook_MFP' => $flipbookData->Flipbook_MFP,
                'Flipbook_SGC' => $flipbookData->Flipbook_SGC,
                'Flipbook_NC' => $flipbookData->Flipbook_NC,
                'Flipbook_Hotel' => $flipbookData->Flipbook_Hotel,
                'Flipbook_Hotel_Date_01' => $flipbookData->Flipbook_Hotel_Date_01,
                'Flipbook_Hotel_DOW' => $flipbookData->Flipbook_Hotel_DOW,

            ]);
        } else {
            User::create([
                'first_name' => $request->first_name,
                'last_name' => $request->last_name,
                'user_name' => $request->user_name,
                'email' => $request->email,
                'role_id' => 2,
                'password' => Hash::make($request->password),
                'BAC_Account_Combined' => $request->BAC_Account_Combined,
                'BAC_Player_ID' => $checkAccount->BAC_Player_ID,
                'BAC_Lname' => $checkAccount->BAC_Lname,
                'BAC_FName' => $checkAccount->BAC_FName,
                'BAC_MI' => $checkAccount->BAC_MI,
                'BAC_Tier' => $checkAccount->BAC_Tier,
                'BAC_Reward_Points' => $checkAccount->BAC_Reward_Points,
                'BAC_Tier_Points' => $checkAccount->BAC_Tier_Points,
                'BAC_Points_Next_Tier' => $checkAccount->BAC_Points_Next_Tier,
                'BAC_Host' => $checkAccount->BAC_Host,
                'BAC_Account_Number' => $checkAccount->BAC_Account_Number,
                'BAC_Temp_Account_Number' => $checkAccount->BAC_Temp_Account_Number,
                'BAC_Host_ID' => $checkAccount->BAC_Host_ID,
                'BAC_DOB' => $request->birthday,
            ]);
        }

        Mail::to($request->email)->send(new WelcomeMail($request));

        return redirect('admin/login')->with('message', 'Register Completed! Please login use your username or your email.');
    }

}
